import argparse
import logging
import csv

from plexapi.audio import Track, Album, Artist
from plexapi.library import MusicSection
from plexapi.myplex import MyPlexAccount

import credentials


def dump_collection_to_file(music: MusicSection, output_file_name: str):
    logging.info(f'Dumping {music} to {output_file_name}')

    with open(output_file_name, mode='w', newline='') as file:
        writer = csv.writer(file, quoting=csv.QUOTE_MINIMAL)

        writer.writerow(['Artist', 'Album', 'Genre', 'Year', 'Disc', 'Track', 'Title', 'Plays', 'Rating', 'Location'])

        for album in music.albums():
            a: Album = album
            artist: Artist = a.artist()
            for track in album.tracks():
                t: Track = track

                writer.writerow(
                    [artist.title, a.title, a.genres[0].tag, a.year, t.parentIndex, t.trackNumber, t.title, t.viewCount,
                     t.userRating,
                     t.locations[0]])


def run(file: str):
    account: MyPlexAccount = MyPlexAccount(credentials.username, credentials.password)
    plex = account.resource(credentials.resource).connect()
    music = plex.library.section('Music')
    dump_collection_to_file(music, file)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser(description="Export collection metadata to CSV")
    parser.add_argument('file', metavar='FILE', type=str, help='CSV file name that will be written')
    args = parser.parse_args()

    run(args.file)
