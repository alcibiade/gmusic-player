from collections import Counter

import numpy
from plexapi.myplex import MyPlexAccount
from plexapi.playlist import Playlist

import credentials
import selector


def create_summary(genre, all_tracks, selected_tracks):
    description = ''
    description += '%d tracks out of %d in %s genre. ' % (len(selected_tracks), len(all_tracks), genre)
    playcounts = [t[1] for t in all_tracks]
    neverplayed = len([c for c in playcounts if c == 0])
    description += 'Average play count is %.1f with %d tracks (%.1f%%) never played.' % (
        numpy.mean(playcounts),
        neverplayed,
        100 * neverplayed / len(all_tracks)
    )

    return description


def run():
    account = MyPlexAccount(credentials.username, credentials.password)
    plex = account.resource(credentials.resource).connect()
    music = plex.library.section('Music')

    genres = set([album.genres[0].tag for album in music.albums()])
    history = plex.history(maxresults=100000)
    playcount = Counter(history)

    playlist_prefix = 'My '
    playlist_size = 100

    for genre in ['Metal']:
        print('Processing genre:', genre)
        p_name = playlist_prefix + genre
        # Extract list of album tracks, each as a list
        track_pool = [a.tracks() for a in music.albums() if genre in [g.tag for g in a.genres]]
        # Flatten tracks from list of lists to a single list
        track_pool = [t for sl in track_pool for t in sl]
        # Wrap tracks as tuples (track, playcount)
        track_pool = [(t, playcount[t]) for t in track_pool]
        print('Track pool size is', len(track_pool))
        p_tracks = selector.select_pool(track_pool,
                                        selector.RandomizedPlayCountSelector(2),
                                        lambda x: x[1],
                                        playlist_size)

        summary = create_summary(genre, track_pool, p_tracks)
        print(summary)

        playlist = [p for p in plex.playlists() if p.title == p_name]
        # if playlist:
        #     playlist = playlist[0]
        #     playlist_items = playlist.items()
        #     print('Removing', len(playlist_items), 'items from playlist', p_name)
        #     for item in playlist_items:
        #         playlist.removeItem(item)
        #
        #     print('Populating playlist', p_name, 'with', len(p_tracks), 'tracks')
        #     playlist.addItems([t[0] for t in p_tracks])
        # else:
        #     Playlist.create(plex, p_name, [t[0] for t in p_tracks], music).edit(title=p_name, summary=summary)


if __name__ == '__main__':
    run()

