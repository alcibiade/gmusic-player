import logging
from datetime import datetime
from collections import Counter

import numpy
from plexapi.myplex import MyPlexAccount
from plexapi.playlist import Playlist

import credentials
import selector


def create_summary(genre, all_tracks, selected_tracks):
    logging.debug('Summary ---------')
    logging.debug('Genre is %s' % genre)
    logging.debug('All tracks size is %d' % len(all_tracks))
    logging.debug('Selected tracks size is %d' % len(selected_tracks))
    logging.debug('Total count %d' % sum([t[1] for t in all_tracks]))

    description = ''
    description += '%d tracks out of %d in %s genre. ' % (len(selected_tracks), len(all_tracks), genre)
    playcounts = [t[1] for t in all_tracks]
    neverplayed = len([c for c in playcounts if c == 0])
    description += 'Average play count is %.1f with %d tracks (%.1f%%) never played. ' % (
        numpy.mean(playcounts),
        neverplayed,
        100 * neverplayed / len(all_tracks)
    )

    current_date = datetime.today()

    # Format the current date as a string in "dd/mm/yyyy" format
    date_string = current_date.strftime("%d/%m/%Y")

    description += f'Playlist generated on {date_string}'

    logging.debug(description)
    logging.debug('-----------------')

    return description


def run():
    account = MyPlexAccount(credentials.username, credentials.password)
    plex = account.resource(credentials.resource).connect()
    music = plex.library.section('Music')

    genres = set([album.genres[0].tag for album in music.albums()])
    history = plex.history(maxresults=100000)

    log_history(history)

    playcount_tracks = Counter([h.key for h in history if h.key])
    playcount_albums = Counter([t.parentKey for t in history if t.parentKey])
    playcount_artists = Counter([t.grandparentKey for t in history if t.grandparentKey])

    playlist_prefix = 'My '
    playlist_size = 100

    for genre in genres:
        logging.info('Processing genre: %s' % genre)
        p_name = playlist_prefix + genre
        # Extract albums
        albums = [a for a in music.albums() if genre in [g.tag for g in a.genres]]
        # Extract list of album tracks, each as a list
        track_pool = [a.tracks() for a in albums]
        # Flatten tracks from list of lists to a single list
        track_pool = [t for sl in track_pool for t in sl]
        # Wrap tracks as tuples (track, playcount_tracks)
        track_pool = [(t, playcount_tracks[t.key], playcount_albums[t.parentKey], playcount_artists[t.grandparentKey])
                      for t in track_pool]
        logging.info('Track pool size is %d' % len(track_pool))
        p_tracks = selector.select_pool(track_pool,
                                        selector.RandomizedPlayCountSelector(2),
                                        lambda x: x[1] + x[2] + x[3],
                                        playlist_size)

        summary = create_summary(genre, track_pool, p_tracks)

        playlist = [p for p in plex.playlists() if p.title == p_name]
        if playlist:
            playlist = playlist[0]
            playlist_items = playlist.items()
            logging.info('Removing %d items from playlist %s' % (len(playlist_items), p_name))
            playlist.removeItems(playlist_items)

            logging.info('Populating playlist %s with %d tracks' % (p_name, len(p_tracks)))
            playlist.addItems([t[0] for t in p_tracks])
            playlist.edit(title=p_name, summary=summary)
        else:
            Playlist.create(plex, p_name, [t[0] for t in p_tracks], music).edit(title=p_name, summary=summary)


def log_history(history):
    logging.debug('History size: %d' % len(history))
    for ix, item in enumerate(history[:10]):
        logging.debug('History %8d: %s' % (ix, item))
    logging.debug('...')
    for ix, item in enumerate(history[-10:]):
        logging.debug('History %8d: %s' % (len(history) - 10 + ix, item))


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    run()
