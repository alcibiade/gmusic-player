# Track selection algorithms
from random import Random
from typing import List, Any, Tuple, Optional


class Selector:
    def select(self, tracks: List[Tuple[Any, Any]], pc: callable) -> int:
        return 0


class RandomSelector(Selector):
    def __init__(self):
        self._rand = Random()

    def select(self, tracks: List[Tuple[Any, Any]], pc: callable = None) -> Optional[int]:
        if len(tracks) == 0:
            return None
        else:
            return self._rand.randrange(0, len(tracks))


class PlayCountSelector(Selector):
    def __init__(self):
        self._randselect = RandomSelector()

    def select(self, tracks: List[Tuple[Any, Any]], pc: callable) -> int:
        c = 0
        result = None

        while len(tracks) > 0 and result is None:
            s_tracks = [t for t in tracks if pc(t) == c]
            if len(s_tracks) > 0:
                selected_index = self._randselect.select(s_tracks)
                selected_track = s_tracks[selected_index]
                result = tracks.index(selected_track)
            else:
                c += 1

        return result


class RandomizedPlayCountSelector(Selector):
    def __init__(self, factor=1.0):
        self._rand = Random()
        self._factor = factor

    def select(self, tracks: List[Tuple[Any, Any]], pc: callable) -> Optional[int]:
        if len(tracks) == 0:
            return None
        else:
            maxPlayCount = max([pc(t) for t in tracks])
            totalPlayCount = sum([pow(1 + maxPlayCount - pc(t), self._factor) for t in tracks])
            targetPlayCount = self._rand.randrange(0, totalPlayCount)

            ix = 0
            for t in tracks:
                targetPlayCount -= pow(1 + maxPlayCount - pc(t), self._factor)

                if targetPlayCount < 0:
                    return ix

                ix += 1

            return None


def select_pool(library: List[Tuple[Any, Any]], selector: Selector, pc: callable, count: int) -> List[Tuple[Any, Any]]:
    candidates = list(library)
    selection = []

    while count > 0 and len(candidates) > 0:
        i = selector.select(candidates, pc)
        selected = candidates[i]
        selection.append(selected)
        candidates.remove(selected)
        count -= 1

    return selection
