## Google Play Music Player

A custom command-line music player serving content from Google Play Music.


## Random playback by genre

The play-random will play random songs from the library. It uses **gstreamer** for playback.

An optional genre can be provided as parameter to the script to filter the music style.

```
yk@triton:~/Documents/gmusic-player$ python3 src/play-random.py jazz
Loading library...
Loaded 10406 tracks
Filter by genre: jazz
Selected 514 tracks
Playing Player                           by Hiromi               from Alive (2014)
Playing What Is There To Say             by Alan Broadbent       from Over The Fence (1994)
Playing Folk Print                       by Sylvain Luc          from Joko (2006)
Playing Sonnymoon                        by Fourplay             from Esprit De Four (2012)
Playing part II.b                        by Keith Jarrett        from The Köln Concert (1975)
...
```

## Generate dynamic playlists

Generate playlists by genre, in a pseudo-random fashion where selection is done based on your play history.

```
yk@triton:~/Documents/gmusic-player$ python3 src/dynamic-playlists.py
Removing existing playlist My Soundtrack
Removing existing playlist My Classical
Removing existing playlist My Dance
Removing existing playlist My Celtic
Removing existing playlist My Ambient
Removing existing playlist My Chanson
Removing existing playlist My Rock
Creating playlist My Progressive
Creating playlist My Comedy
Creating playlist My Psychedelic
Creating playlist My Rock
Creating playlist My Fusion
Creating playlist My Classical
Creating playlist My Chanson
Creating playlist My Dance
Creating playlist My Holiday
Creating playlist My Soundtrack
Creating playlist My Pop
Creating playlist My Celtic
Creating playlist My Ambient
Creating playlist My Metal
Creating playlist My Blues
Creating playlist My Jazz
Creating playlist My Electronic
```