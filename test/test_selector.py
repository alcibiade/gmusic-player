import unittest

from selector import RandomSelector, PlayCountSelector, select_pool, RandomizedPlayCountSelector


class SelectorTest(unittest.TestCase):
    def testRandomSelector(self):
        library = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        selector = RandomSelector()

        self.assertEqual(None, selector.select([]))

        for i in range(20):
            ix = selector.select(library)
            self.assertTrue(ix >= 0 and ix < len(library))

    def testPlayCountSelector(self):
        library = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        selector = PlayCountSelector()

        self.assertEqual(None, selector.select([], lambda x: x))

        for i in range(20):
            ix = selector.select(library, lambda x: x)
            self.assertTrue(ix >= 0 and ix < len(library))

    def testRandomizedPlayCountSelector(self):
        library = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        selector = RandomizedPlayCountSelector()

        self.assertEqual(None, selector.select([], lambda x: x))

        for i in range(20):
            ix = selector.select(library, lambda x: x)
            self.assertTrue(ix >= 0 and ix < len(library))

    def testPooling(self):
        library = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        selector = PlayCountSelector()
        tracks = select_pool(library, selector, lambda x: x, 4)
        self.assertEqual(len(tracks), 4)

    def testAgainRandomizedPlayCountSelector(self):
        maxcount = 6

        library = [{'playCount': i} for i in range(0, maxcount)]
        selections = {i: 0 for i in range(0, maxcount)}

        selector = RandomizedPlayCountSelector(1)

        for i in range(10000):
            ix = selector.select(library, lambda x: x['playCount'])
            selections[ix] += 1

        print(selections)
        self.assertNotIn(0, selections.values())
        self.assertGreater(selections[0], selections[1])
