FROM python:3.10

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN mkdir /export

CMD python3 ./src/export.py /export/music_collection.csv && python3 ./src/dynamic-playlists.py

